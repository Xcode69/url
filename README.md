POLÍTICA DE PRIVACIDAD
El presente Política de Privacidad establece los términos en que Fuerteventura Waves Map usa y protege 
la información que es proporcionada por sus usuarios al momento de utilizar la app. Esta compañía está 
comprometida con la seguridad de los datos de sus usuarios, lo hacemos asegurando que sólo se empleará de acuerdo con los términos de este documento. 
Sin embargo esta Política de Privacidad puede cambiar con el tiempo o ser actualizada por lo que le recomendamos y enfatizamos revisar 
continuamente esta página para asegurarse que está de acuerdo con dichos cambios.
Información que es recogida
Localizacion geografica del dispositivo para el correcto uso del mapa.

Uso de la información recogida
Nuestra app  emplea la información con el fin de proporcionar el mejor servicio posible. 

Fuerteventura Waves Map Se reserva el derecho de cambiar los términos de la presente Política de Privacidad en cualquier momento.
